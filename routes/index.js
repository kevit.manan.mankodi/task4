var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.send('<a href="/users">Sign In</a> <a href="/users/login">Log In</a>');
});

module.exports = router;
