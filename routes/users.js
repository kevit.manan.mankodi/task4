var express = require('express');
const multer = require('multer')
const sharp = require('sharp')
const fs = require('fs')
const path = require('path')
const User = require('../models/user')
const auth = require('../middleware/auth')
const upload = require('../middleware/uploadImage')
var router = express.Router();

router.post('/', async (req, res) => {
  const user = new User(req.body)

  try {
    // console.log('inside sign in')
    await user.save()
    const token = await user.generateAuthToken()
    // console.log(user)
    res.status(201).send({ user, token })
    console.log('Sign up succesful')
  } catch (e) {
    console.log(e)
    res.status(400).send(e)
  }

})

router.post('/login', async (req, res) => {
  try {
    const user = await User.findByCredentials(req.body.email, req.body.password)
    const token = await user.generateAuthToken()
    res.send({ user, token })
    console.log('Log in succesful')
  } catch (e) {
    console.log(e)
    res.status(400).send()
  }
})

router.post('/logout', auth, async (req, res) => {
  try {
    req.user.tokens = req.user.tokens.filter((token) => {
      return token.token !== req.token
    })
    await req.user.save()

    res.send()
    console.log('Log out successful')
  } catch (e) {
    console.log(e)
    res.status(500).send()
  }
})

router.post('/logoutAll', auth, async (req, res) => {
  try {
    req.user.tokens = []
    await req.user.save()

    res.send()
    console.log('Logged out from all places')
  } catch (e) {
    console.log(e)
    res.status(500).send()
  }
})

router.get('/me', auth, async (req, res) => {
  res.send(req.user)
  console.log('Displayed user\'s details')
})

router.get('/:id', auth, async (req, res) => {
  const user = await User.findById(req.params.id)
  const userObject = await user.readOthersInfo()
  res.send({ userObject })
  console.log('Displayed some other users\' details')
})

router.patch('/me', auth, async (req, res) => {
  const updates = Object.keys(req.body)
  const allowedUpdates = ['name', 'email', 'password', 'address', 'phoneNumber']
  const isValidOperation = updates.every((update) => allowedUpdates.includes(update))

  if (!isValidOperation) {
    return res.status(400).send({ error: 'Invalid updates!' })
  }

  try {

    updates.forEach((update) => req.user[update] = req.body[update])
    await req.user.save()


    res.send(req.user)
    console.log('Update successful')

  } catch (e) {
    console.log(e)
    res.status(400).send(e)
  }

})

router.delete('/me', auth, async (req, res) => {
  try {
    fs.unlink(path.join(__dirname, `../profilePictures/${req.user.id}.png`), (err) => {
      if (err) throw err
      console.log('Deleted profile picture successfully.')
    } )
    await req.user.remove()
    res.send(req.user)
    console.log('User deleted successfully')
  } catch (e) {
    console.log(e)
    res.status(500).send()
  }
})



router.post('/me/profilePicture', auth, upload.single('profilePicture'), async (req, res) => {
  // console.log('inside route')
  const buffer = await sharp(req.file.buffer).resize({ width: 250, height: 250 }).png().toBuffer()

  fs.writeFile(path.join(__dirname, `../profilePictures/${req.user._id}.png`), buffer, 'binary', (err) => {
      if (err) throw err
      console.log('File saved locally succesfully')
  })

   req.user.profilePicture = `/users/${req.user.id}/profilePicture`
  await req.user.save()
  res.send({ url: process.env.BASE_URL + req.user.profilePicture })
  console.log('Uploaded profile picture successfully')
}, (error, req, res, next) => {
  console.log(error)
  res.status(400).send({ error: error.message })
})

router.delete('/me/profilePicture', auth, async (req, res) => {
  req.user.profilePicture = undefined
  fs.unlink(path.join(__dirname, `../profilePictures/${req.user.id}.png`), (err) => {
    if (err) throw err
    console.log('Deleted profile picture successfully.')
  } )
  await req.user.save()
  res.send()
})

router.get('/:id/profilePicture', async (req, res) => {
  try {
    const user = await User.findById(req.params.id)

    if (!user || !user.profilePicture) {
      throw new Error('User or profile picture not found!')
    }

    res.set('Content-Type', 'image/png')
    res.sendFile(path.join(__dirname, `../profilePictures/${req.params.id}.png`))
    console.log('Profile picture displayed successfully')
  } catch (e) {
    console.log(e)
    res.status(404).send()
  }
})

module.exports = router;
