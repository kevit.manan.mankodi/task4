const multer = require('multer')
const path = require('path')


  const upload = multer({
  
  
    limits: {
      fileSize: 10000000
    },
  
    async fileFilter(req, file, cb) {
      // console.log('inside multer')
      
      if (!file.originalname.match(/\.(jpg|jpeg|png)$/)) {
        return cb(new Error('Please upload an image'))
      }
      
      cb(undefined, true)
    }
  
  })

  module.exports = upload