const express = require('express')
require('./db/mongoose')
// const indexRouter = require('./routes/index')
const userRouter = require('./routes/users')


const app = express()
const port = process.env.PORT || 3000



app.use(express.json())

// app.use('/', indexRouter)
app.use('/users', userRouter)



// app.listen(port, () => {
//   console.log('Server is up on port ' + port)
// })

module.exports = app