const mongoose = require('mongoose')

const dbURL = process.env.DB_URL || 'mongodb://127.0.0.1:27017/task4'

mongoose.connect(dbURL, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false
  })