const mongoose = require('mongoose')
const validator = require('validator')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')



const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        trim: true
    },
    email: {
        type: String,
        required: true,
        trim: true,
        unique: true,
        lowercase: true,
        validate(value) {
            if (!validator.isEmail(value)) {
                throw new Error('Email is invalid!')
            }
        }
    },
    password: {
        type: String,
        required: true,
        trim: true,
        minlength: 7
    },
    address: {
        type: String,
        required: true,
        trim: true
    },
    phoneNumber: {
        type: Number,
        required: true,
        trim: true,
        validate(value) {
            value = value.toString()
            if (!validator.isMobilePhone(value)) {
                throw new Error('Phone Number is invalid!')
            }
        }
    },
    tokens: [{
      token: {
        type: String,
        required: true
      }
    }],
    profilePicture: {
        type: String
    }
})

userSchema.methods.toJSON = function () {
    const user = this
    const userObject = user.toObject()
    userObject.profilePicture = process.env.BASE_URL + userObject.profilePicture
    delete userObject.password
    delete userObject.tokens
    //delete userObject.profilePicture

    return userObject
}

userSchema.methods.readOthersInfo = function () {
    const user = this
    const userObject = user.toObject()
    userObject.profilePicture = process.env.BASE_URL + userObject.profilePicture
    delete userObject.password
    delete userObject.tokens
    delete userObject._id
    delete userObject.email
    delete userObject.address
    delete userObject.phoneNumber
    //delete userObject.profilePicture

    return userObject
}



userSchema.methods.generateAuthToken = async function () {
    const user = this
    //console.log(process.env.JWT_SECRET, process.env.PORT)
    const token = jwt.sign({_id: user._id.toString()}, process.env.JWT_SECRET)

    user.tokens = user.tokens.concat({ token })
    await user.save()

    return token
}

userSchema.statics.findByCredentials = async (email, password) => {
    const user = await User.findOne({ email })

    if(!user) {
        throw new Error('Unable to login')
    }

    const isMatch = await bcrypt.compare(password, user.password)

    if (!isMatch) {
        throw new Error('Unable to login')
    }

    return user
}

userSchema.pre('save', async function (next) {
    const user = this

    if (user.isModified('password')) {
        user.password = await bcrypt.hash(user.password, 8)
    }

    next()
})

const User = mongoose.model('User', userSchema)

module.exports = User

